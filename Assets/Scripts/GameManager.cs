﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Game Checklist:
 * Advertisments - Unity and Admob
 * Advertisment handles
 * Google Play Services - Achievements and Leaderboards
 * Social App integration (optional)
 * Finiance - In App Purchases (optional)
 * Google Play - Publish game
 * */

public class GameManager : MonoBehaviour {
    int sceneIndex;
    UnityAdManager unityAd;
    AdMobManager adMob;

    /*private void Awake()
    {
        unityAd = GameObject.Find("UnityAdManager").GetComponent<UnityAdManager>();
        unityAd.DisplayUnityInterstitial();
    }*/

    // Use this for initialization
    void Start () {
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        unityAd = GameObject.Find("UnityAdManager").GetComponent<UnityAdManager>();
        adMob = GameObject.Find("AdMobManager").GetComponent<AdMobManager>();
        adMob.RequestAdMobBanner();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CompleteLevel()
    {
        PlayerPrefs.SetInt("Level" + sceneIndex, 1);
        PlayerPrefs.SetInt("Level" + --sceneIndex + "_score", UIManager.score);
        unityAd.DisplayUnityInterstitial();
        SceneManager.LoadScene(1);
        ActivateAchievements(sceneIndex, UIManager.score);
        UIManager.score = 0;
    }

    public void RestartScene()
    {
        UIManager.score = 0;
        SceneManager.LoadScene(sceneIndex);
    }

    private void ActivateAchievements(int scene, int score)
    {
        switch (sceneIndex)
        {
            case 1: {
                    GoogleGamesController.UnlockAchievement(GPGSIds.achievement_complete_level_1);
                    if (score >= 100)
                    {
                        GoogleGamesController.UnlockAchievement(GPGSIds.achievement_level_1_master);
                    }
                    break;
                }
            case 2:
                {
                    GoogleGamesController.UnlockAchievement(GPGSIds.achievement_complete_level_2);
                    if (score >= 100)
                    {
                        GoogleGamesController.UnlockAchievement(GPGSIds.achievement_level_2_master);
                    }
                    break;
                }

            case 3:
                {
                    GoogleGamesController.UnlockAchievement(GPGSIds.achievement_complete_level_3);
                    if (score >= 100)
                    {
                        GoogleGamesController.UnlockAchievement(GPGSIds.achievement_level_3_master);
                    }
                    break;
                }

            case 4:
                {
                    GoogleGamesController.UnlockAchievement(GPGSIds.achievement_complete_level_4);
                    if (score >= 100)
                    {
                        GoogleGamesController.UnlockAchievement(GPGSIds.achievement_level_4_master);
                    }
                    break;
                }

            case 5:
                {
                    GoogleGamesController.UnlockAchievement(GPGSIds.achievement_complete_level_5);
                    if (score >= 100)
                    {
                        GoogleGamesController.UnlockAchievement(GPGSIds.achievement_level_5_master);
                    }
                    break;
                }

            default: break;
        }
    }
}
