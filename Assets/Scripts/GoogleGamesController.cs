﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class GoogleGamesController : MonoBehaviour {

    public static bool isLoggedIn;

	// Use this for initialization
	void Start () {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        SignIn();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SignIn()
    {
        if (!isLoggedIn)
        {
            Social.localUser.Authenticate(success => { });
        }
    }

    public void SignOut()
    {
        PlayGamesPlatform.Instance.SignOut();
    }

    #region Achievements
    public static void UnlockAchievement(string id)
    {
        Social.ReportProgress(id, 100, success => { });
    }

    public static void IncrementalAchievement(string id, int stepsToIncrement)
    {
        PlayGamesPlatform.Instance.IncrementAchievement(id, stepsToIncrement, success => { });
    }

    public static void ShowAchievements()
    {
        Social.ShowAchievementsUI();
    }
    #endregion Achievements

    #region Leaderboards
    public static void ShowLeaderboards()
    {
        Social.ShowLeaderboardUI();
    }

    public static void AddScoreToLeaderboard(string leaderboardId, long score)
    {
        Social.ReportScore(score, leaderboardId, success => { });
    }
    #endregion Leaderboards
}
