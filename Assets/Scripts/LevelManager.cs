﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    //public List<LevelButton> levelButton;

    [System.Serializable]
    public class Level
    {
        public string levelText;
        public int isUnlocked; // 0 = locked, 1 = unlocked
        public bool isInteractable;

        //public Button.ButtonClickedEvent OnClickEvent;
    }

    public GameObject levelButton;
    public Transform spacer;
    public List<Level> levelList;

	// Use this for initialization
	void Start () {
        //DeleteAll();
        FillList();
	}

    void FillList()
    {
        foreach(var level in levelList)
        {
            GameObject newButton = Instantiate(levelButton) as GameObject;
            LevelButton button = newButton.GetComponent<LevelButton>();
            button.levelText.text = level.levelText;

            if (PlayerPrefs.GetInt("Level" + button.levelText.text) == 1)
            {
                level.isUnlocked = 1;
                level.isInteractable = true;
            }

            button.isUnlocked = level.isUnlocked;
            button.GetComponent<Button>().interactable = level.isInteractable;
            button.GetComponent<Button>().onClick.AddListener(() => LoadLevels("Level" + button.levelText.text));

            if(PlayerPrefs.GetInt("Level" + button.levelText.text + "_score") == 100)
            {
                button.star1.SetActive(true);
                button.star2.SetActive(true);
                button.star3.SetActive(true);
            } else if(PlayerPrefs.GetInt("Level" + button.levelText.text + "_score") >= 50)
            {
                button.star1.SetActive(true);
                button.star2.SetActive(true);
            } else if (PlayerPrefs.GetInt("Level" + button.levelText.text + "_score") > 0)
            {
                button.star1.SetActive(true);
            }

            newButton.transform.SetParent(spacer);
        }

        //SaveAll();
    }

    void LoadLevels(string value)
    {
        SceneManager.LoadScene("scenes/" + value);
    }

    void SaveAll()
    {
        // Note: If wanting to add more levels, need to change this as it will only save levels the first time, could change it to last level saved instead
        if(PlayerPrefs.HasKey("Level5"))
        {
            return;
        } else
        {
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("LevelButton");

            foreach (GameObject btn in buttons)
            {
                LevelButton lvlBtn = btn.GetComponent<LevelButton>();
                PlayerPrefs.SetInt("Level" + lvlBtn.levelText.text, lvlBtn.isUnlocked);
            }
        }
    }

    void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }

}
