﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour {
    public Text coinText;
    public Text colorChangeText;
    public GameObject snowball;
    private int amount;

	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        amount = PlayerPrefs.GetInt("reward_coins");
        coinText.text = ": " + amount;
	}

    public void ChangeToGreen()
    {
        if(amount <= 0)
        {
            StartCoroutine(ShowColorChangeMessage("You do not have enough rewards coins"));
        } else
        {
            amount -= 10;
            PlayerPrefs.SetInt("reward_coins", amount);
            snowball.gameObject.GetComponent<Renderer>().sharedMaterial.color = Color.green;
            StartCoroutine(ShowColorChangeMessage("Snowball colour is now green"));
        }
    }

    public void ChangeToWhite()
    {
        if (amount <= 0)
        {
            StartCoroutine(ShowColorChangeMessage("You do not have enough rewards coins"));
        } else
        {
            amount -= 10;
            PlayerPrefs.SetInt("reward_coins", amount);
            snowball.gameObject.GetComponent<Renderer>().sharedMaterial.color = Color.white;
            StartCoroutine(ShowColorChangeMessage("Snowball colour is now white"));
        }
    }

    public void ChangeToOrange()
    {
        if (amount <= 0)
        {
            StartCoroutine(ShowColorChangeMessage("You do not have enough rewards coins"));
        } else
        {
            amount -= 10;
            PlayerPrefs.SetInt("reward_coins", amount);
            snowball.gameObject.GetComponent<Renderer>().sharedMaterial.color = new Color(255, 197, 9);
            StartCoroutine(ShowColorChangeMessage("Snowball colour is now orange"));
        }
    }

    IEnumerator ShowColorChangeMessage(string message)
    {
        colorChangeText.text = message;
        colorChangeText.enabled = true;
        yield return new WaitForSeconds(2.0f);
        colorChangeText.enabled = false;
    }
}
