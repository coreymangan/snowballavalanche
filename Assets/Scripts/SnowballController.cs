﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowballController : MonoBehaviour {
    public Rigidbody rb;
    GameManager gm;
    UIManager ui;

    //private Vector3 moveDir = Vector3.zero;
    private float horSpeed = 25.0f;
    private float verSpeed = 3.5f;

	// Use this for initialization
	void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        ui = GameObject.Find("UIManager").GetComponent<UIManager>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //Movement PC
        /*float moveHor = Input.GetAxis("Horizontal") * horSpeed * Time.deltaTime;
        float moveVer = Input.GetAxis("Vertical") * verSpeed * Time.deltaTime;
        moveDir = new Vector3(moveHor, 0, moveVer);

        rb.AddForce(moveDir);*/

        //Movement Phone
        float moveHor = Input.acceleration.x * horSpeed * Time.deltaTime;

        //moveDir = new Vector3(moveHor, 0, verSpeed);
        //transform.Translate(new Vector3(moveHor, 0, 0));
        transform.position += new Vector3(moveHor, 0, 0);
        rb.AddForce(new Vector3(0, 0, verSpeed));

        transform.localScale += new Vector3(0.02f, 0.02f, 0.02f) * Time.deltaTime;
        Debug.DrawRay(transform.position, transform.forward);
	}

    void OnCollisionEnter(Collision col)
    {
        Debug.Log(col.gameObject);

        if(col.collider.tag == "Obstacle")
        {
            Debug.Log("Game Over");
            ui.GameOver();
        } else if(col.collider.tag == "Finish")
        {
            Debug.Log("Level Completed");
            gm.CompleteLevel();
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Coin")
        {
            Debug.Log("Coin Collected");
            UIManager.score += 10;
            GoogleGamesController.AddScoreToLeaderboard(GPGSIds.leaderboard_snowball_avalanche_leaderboard, UIManager.score);
            this.gameObject.GetComponent<AudioSource>().Play();
            Destroy(col.gameObject);
        }
    }
}
