﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class UIManager : MonoBehaviour {
    public GameObject pausePanel;
    public bool isPaused;
    public Text scoreText;
    public Text gameOverText;
    public Text rewardCoinText;
    public static int score;
    public static int rewardCoins;

    public Canvas menuCanvas;
    public Canvas shopCanvas;
    public Canvas settingsCanvas;

    // Use this for initialization
    void Start () {
        rewardCoins = PlayerPrefs.GetInt("reward_coins");

        if (menuCanvas)
        {
            this.menuCanvas = this.menuCanvas.GetComponent<Canvas>();
            this.shopCanvas = this.shopCanvas.GetComponent<Canvas>();
            this.settingsCanvas = this.settingsCanvas.GetComponent<Canvas>();
        }
    }

    // Update is called once per frame
    void Update () {
		if(pausePanel)
        {
            this.CheckPaused();
        }

        if (scoreText)
        {
            scoreText.text = "Score: " + score;
        }

        if (rewardCoinText)
        {
            rewardCoinText.text = ": " + rewardCoins;
        }
	}

    void CheckPaused()
    {
        if (isPaused)
        {
            PauseGame(true);
        }
        else
        {
            PauseGame(false);
        }
    }

    void PauseGame(bool state)
    {
        if (state)
        {
            Time.timeScale = 0.0f;
            this.pausePanel.SetActive(true);
        } else
        {
            Time.timeScale = 1.0f;
            this.pausePanel.SetActive(false);
        }
    }

    public void SwitchPause()
    {
        this.isPaused = !this.isPaused;
    }

    public void ExitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #endif

        Application.Quit();
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void GameOver()
    {
        Debug.Log("Game over exiting scene");
        gameOverText.enabled = true;
        score = 0;
        Invoke("ExitLevel", 1.0f);
    }

    public void ExitLevel()
    {
        SceneManager.LoadScene(1);
    }

    public void DisplayAchievements()
    {
        GoogleGamesController.ShowAchievements();
    }

    public void DisplayLeaderboard()
    {
        GoogleGamesController.ShowLeaderboards();
    }

    // main menu items

    public void OpenMenu()
    {
        this.menuCanvas.enabled = true;
        this.settingsCanvas.enabled = false;
        this.shopCanvas.enabled = false;
    }

    public void OpenSettings()
    {
        this.settingsCanvas.enabled = true;
        this.menuCanvas.enabled = false;
        this.shopCanvas.enabled = false;
    }

    public void OpenShop()
    {
        this.shopCanvas.enabled = true;
        this.menuCanvas.enabled = false;
        this.settingsCanvas.enabled = false;
    }

    // settings items


}
