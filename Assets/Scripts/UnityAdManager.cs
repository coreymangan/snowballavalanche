﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
//using UnityEngine.Monetization;

public class UnityAdManager : MonoBehaviour {
    private bool testMode = true;
    public Text adText;

    #if UNITY_IOS
        public const string gameID = "3058468";
    #elif UNITY_ANDROID
        public const string gameID = "3058468";
    #elif UNITY_EDITOR
        public const string gameID = "1111111";
    #endif

    // Use this for initialization
    void Start () {
        Advertisement.Initialize(gameID, testMode);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DisplayUnityRewardedVideo()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowRewardResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    /*public void DisplayUnityBanner()
    {
        if (Advertisement.IsReady("banner"))
        {
            Debug.Log("ad ready");
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("banner", options);
        }
    }*/

    public void DisplayUnityInterstitial()
    {
        if (Advertisement.IsReady("Interstitial"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("Interstitial", options);
        }
    }

    public void DisplayUnityVideo()
    {
        if (Advertisement.IsReady("video"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("video", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                //StartCoroutine(this.ShowMessage("Here is your reward!"));
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    private void HandleShowRewardResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                UIManager.rewardCoins += 10;
                PlayerPrefs.SetInt("reward_coins", UIManager.rewardCoins);
                //StartCoroutine(this.ShowMessage("Here is your reward!"));
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    private IEnumerator ShowMessage(string message)
    {
        Debug.Log("here");
        adText.text = message;
        adText.enabled = true;
        yield return new WaitForSeconds(3);
        adText.enabled = false;
    }
}
