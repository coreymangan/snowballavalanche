﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraPos : MonoBehaviour {
    private GameObject snowball;
    public Transform pivot;
    private float camDistanceZ = 2.5f;
    private float camDistanceY = 1.5f;
    private float camRotate = 10.0f;
    // private Transform camPos;
    //private Transform snowballPos;
    //private Quaternion prevRot;

    // Use this for initialization
    void Start () {
        //snowballPos = snowball.transform;
        snowball = GameObject.FindGameObjectWithTag("Snowball");
        //prevRot = pivot.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        //pivot.rotation = Quaternion.identity;

        //Vector3 snowballVel = snowball.GetComponent<Rigidbody>().velocity.normalized;
        //snowballVel.y = 0;

        //transform.forward = snowballVel;
        //transform.forward = pivot.transform.forward;

        transform.rotation = Quaternion.Euler(camRotate, 0, 0);
        transform.position = new Vector3(snowball.transform.position.x, snowball.transform.position.y + camDistanceY, snowball.transform.position.z - camDistanceZ);
        camDistanceZ += 0.05f * Time.deltaTime;
        camDistanceY += 0.05f * Time.deltaTime;
        camRotate += 0.1f * Time.deltaTime;

        //pivot.rotation = Quaternion.Euler(new Vector3(0, snowballVel.x, 0));
        //pivot.forward = snowballVel;
        //pivot.rotation = Quaternion.Lerp(prevRot, pivot.rotation, 0.5f);
        //prevRot = pivot.rotation;

        //prevRot = Quaternion.LookRotation(snowballVel, Vector3.up);

        //pivot.rotation = Quaternion.Lerp(prevRot, pivot.rotation, 0.9f);
        //pivot.rotation = Quaternion.Euler(0, pivot.rotation.eulerAngles.y, 0);
    }

    void RotateCameraTopView()
    {
        transform.rotation = Quaternion.Lerp(Quaternion.identity, Quaternion.Euler(10, 45, 0), 0.5f);
    }
}
